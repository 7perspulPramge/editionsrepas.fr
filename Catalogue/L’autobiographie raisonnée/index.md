---
Collection: Presses de l'Economie Sociale
DépotLégal: 2017
Auteur: Jean-François Draperi
Titre: L’autobiographie raisonnée
SousTitre: Pratiques et usages
ISBN : 978-2-918522-05-8
Pages: 343 pages
Prix: 16
Etat: Disponible
Résumé: |
    L’autobiographie raisonnée est un exercice inventé par Henri Desroche (1914-1994) au cours des années 1970 en vue de soutenir des adultes s’engageant dans un cursus de formation supérieure.

    Le succès de cet exercice a incité de nombreux acteurs sociaux à le pratiquer pour d’autres fins et dans des cadres professionnels multiples. Une formation spécifique a été conçue par Jean-François Draperi au Centre d’économie sociale (Cestes) du Conservatoire national des arts et métiers (le Cnam). Elle est délivrée chaque année depuis 2005. Des stagiaires qui ont suivi cette formation et initié de nouvelles applications de l’autobiographie raisonnée se rencontrent depuis trois ans au sein de l’association Acte 1 (Acteurs, chercheurs, territoires d’économie sociale).

    Ce livre rend compte de ces pratiques à partir de la présentation et de l’analyse de dix expériences. Les usages présentés sont multiples : organisation collective du travail, valorisation des compétences, formation supérieure, réinsertion professionnelle, orientation, réorganisation institutionnelle. Ces usages ne sont pas sans transformer la pratique originelle. L’ouvrage donne à voir ces transformations et l’intérêt de l’autobiographie raisonnée pour l’accomplissement personnel comme pour fonder une économie humaine.

    Cet ouvrage a été publié avec le soutien de l’association Acte 1 (Acteurs, chercheurs, territoires d’économie sociale).
Tags:
- Economie sociale et solidaire
- Analyse de pratique
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/l-autobiographie-raisonnee-pratiques-et-usages-jean-francois-draperi?_ga=2.147601463.1091905683.1671446978-84470218.1590573003
Couverture:
Couleurs:
  Fond: '#C91A1E' #ou essayer #fc565b
  PageTitres: '#ab1116'
  Texte: '#fff4fa'
  Titre: 'fff'
Fonts:
  Titre: mermaid
  SousTitre: inherit
---
