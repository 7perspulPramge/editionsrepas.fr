---
Edition: Editions REPAS
DépotLégal: 2016
Autrice: Anne Bruneau
Titre: Commun Village
SousTitre: 40 ans d'aventures en habitat participatif
Préface: Préface d'Isabelle Rey-Lefebvre
Collection: Collection Pratiques Utopiques
ISBN: 978-2-919272-10-5
Pages: 144 pages
Prix: 17
Etat: Disponible
Résumé: |
    À la fin des années soixante-dix, un groupe de trentenaires rêve de créer un habitat associant logement individuel et espaces communs. Ces jeunes adultes souhaitent changer la vie et inventent au fil des années l’habitat groupé autogéré, qui deviendra l’habitat participatif.

    À travers les voix de Jean, Nadia, Hubert, Aminata, et les autres, entrez par la petite porte du Hangar, partagez le parcours de ce groupe emblématique et suivez l’aventure d’une génération innovante et joyeusement utopiste.
Tags:
- Habitat participatif
- Collectif
- Autogestion
SiteWeb: http://www.ecohabitatgroupe.fr/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/commun-village-anne-bruneau-l-association-eco-habitat-groupe?_ga=2.139598899.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272105-commun-village-40-ans-d-aventures-en-habitat-participatif-anne-bruneau/?utm_source=dmg
Couverture:
Couleurs:
  Fond: '#ff8930'
  Titre: '#ffffff'
  Texte: '#ffffff'
  PageTitres: '#eb690b'
Vidéos:
  https://www.youtube.com/watch?v=hl2IpDxysHw: Commun Village, 40 ans d'aventures en habitat participatif
  https://www.youtube.com/watch?v=KOTPqS-npFc: "Radio Arverne : Michel Lulek parle de « Commun Village » des Editions Repas"
  https://www.youtube.com/watch?v=mJVw-S-96MI: "Radio Arverne : Cécile Viallon explique l'habitat participatif"
---

## L'autrice

Issue de la presse régionale et associative, **Anne Bruneau** a écrit *Emigrance* sur l’exil et *Elysez-moi* sur l’utopie politique au sein du collectif d’auteurs de Dailylife.
Au sein de la Fabrique, elle travaille depuis plusieurs années sur le livre et la lecture et a réalisé les films *Histoires de lecteurs*, *Grands Lecteurs* et *Carnet de Campagne* (production Les Docs du Nord).
Au théâtre, elle a signé les textes des pièces *Etranges aCorps* et *Ces mots qui sortent de l’ombre*, écrits à partir de rencontres avec des personnes en situation de handicap et d’illettrisme.
*Commun Village* est un docu-fiction écrit dans une démarche collaborative.

## Extrait

> A l’époque, nous avions compris que si nous ne pouvions pas totalement changer le monde, nous pouvions au moins changer nos vies, enfin, vivre comme nous l’entendions, en instituant une sorte de fraternité du quotidien. Avec deux couples amis, et Martine qui m’avait rejoint dès qu’il avait été entendu que nous ferions un bout de chemin ensemble elle et moi, nous avons imaginé et rêvé nos logements, tous collés les uns aux autres, avec un jardin pour les enfants et une maison commune pour nous réunir, organiser des fêtes, accueillir amis et familles. Certains d’entre nous avaient des expériences similaires, mais plus informelles. D’autres simplement pas envie de vivre une vie comme les autres, chacun chez soi, alors que nous avions partagé l’espoir d’une vie différente, plus gaie, plus collective. Une vie plus grande, engagée, enthousiaste, militante, ouverte aux autres, généreuse en un mot.
> -- page 12
